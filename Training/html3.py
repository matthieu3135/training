#################################################################################################

# This is to check for the creation of chemin3.html

#################################################################################################
import os

# We check if there is html files in our repository
test_HTML = os.listdir()
result = 0

for i in test_HTML:
    if(i == "chemin3.html"):
        result = 1
        break

# If there is chemin3.html, we create a text file, so the pipeline will know that this file is present
if result == 1:
    file = open("html3.txt", "w")
    file.write("chemin3.html is present, so it is a success!")
    file.close()