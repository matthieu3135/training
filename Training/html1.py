#################################################################################################

# This is to check for the creation of chemin1.html

#################################################################################################
import os

# We check if there is html files in our repository
test_HTML = os.listdir()
result = 0

for i in test_HTML:
    if(i == "chemin1.html"):
        result = 1
        break

# If there is chemin1.html, we create a text file, so the pipeline will know that this file is present
if result == 1:
    file = open("html1.txt", "w")
    file.write("chemin1.html is present, so it is a success!")
    file.close()