import os

# We check if there is html files in our repository
test_no_HTML = os.listdir()
result = 0
for i in test_no_HTML:
    if(i[-5:] == ".html"):
        result = 1
        break

# If there is no html files, we create a text file, so the pipeline will know that there isn't html files there
if result == 0:
    file = open("no_html.txt", "w")
    file.write("There is no html files here, so it is a success!")
    file.close()

